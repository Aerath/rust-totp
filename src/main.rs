// Made with the help  of https://jacob.jkrall.net/totp

extern crate hmac_sha1;
extern crate base32;

use std::time::{SystemTime,UNIX_EPOCH};
use std::mem::transmute;

fn main() {
    let args: Vec<_> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Error: 1 argument required, {} given", args.len()-1);
        std::process::exit(-1);
    }
    let secret = args.get(1).unwrap();
    let secret = base32::decode(base32::Alphabet::RFC4648{padding:false}, &secret).unwrap();
    let msg = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs()/30;
    let msg: [u8;8] = unsafe {transmute(msg.to_be())};
    let hs = hmac_sha1::hmac_sha1(&secret, &msg);
    let offset = (hs[hmac_sha1::SHA1_DIGEST_BYTES-1] & 0xF) as usize;
    let dbc = ((hs[offset]as u32)<<24) + ((hs[offset+1]as u32)<<16) + ((hs[offset+2]as u32)<<8) + hs[offset+3]as u32;
    let dbc = (dbc &! (1<<31)).to_string();
    let dbc: String = dbc.chars().skip(dbc.len()-6).take(6).collect();
    println!("{}", dbc);
}
